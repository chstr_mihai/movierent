from Exceptii.exceptii import RepoError

class Repo:

    def __init__(self):
        #functia care creeaza lista de elemente
        self.__elemente = []

    def adauga(self, elem):
        #functia care adauga un element nou in lista

        if elem in self.__elemente:
            raise RepoError("exista un element cu acest id\n")
        self.__elemente.append(elem)
    """
    def cauta(self, key):
        #functia care verifica daca exista un element in lista de obiecte si, in caz afirmativ, il returneaza
        if key not in self.__elemente:
            raise RepoError("id inexistent!\n")
        for x in self.__elemente:
            if x == key:
                return x
    """



    def cauta(self,cheie,x):
        #recursive funciton
        """
        Returneaza in numele functiei id-ul cartii gasite dupa nume
        Input:
            -nume: string, numele cartii cautate
        raises(Erori=carte inexistenta!)
        """
        if x==-1:
            raise RepoError("id inexistent!\n")
        elif self.__elemente[x] == cheie:
            return self.__elemente[x]
        else:
            return self.cauta(cheie, x-1)



    def get_all(self):
        #functia care returneaza lista cu toate elementele
        return self.__elemente[:]

    def size(self):
        #functia care returneaza numarul de elemente din lista
        return len(self.__elemente)

    """
    def delete(self, key):
        #functia care sterge un element din lista
        if key not in self.__elemente:
            raise RepoError("nu exista un element cu acest id\n")
        for x in range(len(self.__elemente)):
            if self.__elemente[x] == key:
                del (self.__elemente[x])
                return
    """

    def delete(self, cheie, x):
        #recursive function
        if x == -1:
            raise RepoError("id inexistent!\n")
        elif self.__elemente[x] == cheie:
            del(self.__elemente[x])
            return
        else:
            return self.delete(cheie, x - 1)

    def empty_list(self):
        self.__elemente = []

    @staticmethod
    def cmp(x, y):
        if x < y:
            return 1
        elif x == y:
            return 0
        return -1