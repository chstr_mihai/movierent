from Infrastructura.repos import Repo
from Domain.entitati import Film, Client, Inchiriere
from Exceptii.exceptii import RepoError


class FilmRepoFromFile(Repo):
    def __init__(self, nume_fisier):
        Repo.__init__(self)
        self.__nume_fisier = nume_fisier
        self.incarca_din_fisier()

    def __creeaza_film(self, linie):
        campuri = linie.split(' ')
        film = Film(int(campuri[0]), campuri[1], campuri[2], campuri[3], 0)
        return film

    def incarca_din_fisier(self):
        Repo.empty_list(self)
        f = open(self.__nume_fisier)
        continut = f.read()
        f.close()
        linii = continut.split('\n')
        for linie in linii:
            if linie.strip() == '':
                continue
            film = self.__creeaza_film(linie)
            Repo.adauga(self, film)


    def __adauga_in_fisier(self, film):
        filme = Repo.get_all(self)

        with open(self.__nume_fisier, 'w') as f:
            for film in filme:
                linie = str(film.get_id()) + ' ' + film.get_titlu() + ' ' + film.get_descriere() + ' ' + film.get_gen()
                f.write(linie)
                f.write('\n')


    def adauga(self, film):
        Repo.adauga(self, film)
        self.__adauga_in_fisier(film)

    def __sterge_din_fisier(self, film):
        with open(self.__nume_fisier, 'r') as f:
            lines = f.readlines()
        with open(self.__nume_fisier, 'w') as f:
            for line in lines:
                if str(line.strip("\n")) != str(film):
                    f.write(line)

    def delete(self, film, x):
        Repo.delete(self, film, x)
        self.__sterge_din_fisier(film)

    def modify(self, film):
        filme = Repo.get_all(self)

        with open(self.__nume_fisier, 'w') as fis:
            for f in filme:
                if f.get_id() == film.get_id():
                    linie = str(film.get_id()) + ' ' + film.get_titlu() + ' ' + film.get_descriere() + ' ' + film.get_gen()
                else:
                    linie = str(f.get_id()) + ' ' + f.get_titlu() + ' ' + f.get_descriere() + ' ' + f.get_gen()
                fis.write(linie)
                fis.write('\n')



class ClientRepoFromFile(Repo):

    def __init__(self, nume_fisier):
        Repo.__init__(self)
        self.__nume_fisier = nume_fisier
        self.__incarca_din_fisier()

    def __creeaza_client(self, linie):
        campuri = linie.split(' ')
        client = Client(int(campuri[0]), campuri[1], campuri[2], 0)
        return client

    def __incarca_din_fisier(self):
        c = open(self.__nume_fisier)
        continut = c.read()
        c.close()
        linii = continut.split('\n')
        for linie in linii:
            if linie.strip() == '':
                continue
            client = self.__creeaza_client(linie)
            Repo.adauga(self, client)

    def __adauga_in_fisier(self, client):
        with open(self.__nume_fisier, 'a') as f:
            linie = str(client.get_id()) + ' ' + client.get_nume() + ' ' + client.get_cnp()
            f.write(linie)
            f.write('\n')

    def adauga(self, client):
        Repo.adauga(self, client)
        self.__adauga_in_fisier(client)

    def cauta(self, element, a):
        clienti = Repo.get_all(self)
        if element not in clienti:
            raise RepoError("id inexistent!\n")
        for x in clienti:
            if x == element:
                return x

    def __sterge_din_fisier(self, client):
        with open(self.__nume_fisier, "r") as f:
            lines = f.readlines()
        with open(self.__nume_fisier, "w") as f:
            for line in lines:
                if str(line.strip("\n")) != str(client):
                    f.write(line)

    def delete(self, client, x):
        Repo.delete(self, client, x)
        self.__sterge_din_fisier(client)

class InchiriereRepoFromFile(Repo):

    def __init__(self, nume_fisier):
        Repo.__init__(self)
        self.__nume_fisier = nume_fisier
        self.__incarca_din_fisier()

    def __creeaza_inchiriere(self, linie):
        campuri = linie.split(' ')
        client = Client(campuri[0], campuri[1], campuri[2], 0)
        film = Film(campuri[3], campuri[4], campuri[5], campuri[6], 0)
        inchiriere = Inchiriere(client, film)
        return inchiriere

    def __incarca_din_fisier(self):
        c = open(self.__nume_fisier)
        continut = c.read()
        c.close()
        linii = continut.split('\n')
        for linie in linii:
            if linie.strip() == '':
                continue
            inchiriere = self.__creeaza_inchiriere(linie)
            Repo.adauga(self, inchiriere)

    def __adauga_in_fisier(self, inchiriere):
        with open(self.__nume_fisier, 'a') as f:
            linie = str(inchiriere.get_client()) + " " + str(inchiriere.get_film())
            f.write(linie)
            f.write('\n')

    def adauga(self, inchiriere):
        Repo.adauga(self, inchiriere)
        self.__adauga_in_fisier(inchiriere)

    def cauta(self, element):
        inchirieri = Repo.get_all(self)
        if element not in inchirieri:
            raise RepoError("id inexistent!\n")
        for x in inchirieri:
            if x == element:
                return x
