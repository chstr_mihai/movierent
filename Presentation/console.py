from Exceptii.exceptii import ValidError, RepoError
import random

class Console:

    def __init__(self, srvFilm, srvClient, srvInchirieri):
        self.__srvFilm = srvFilm
        self.__srvClient = srvClient
        self.__srvInchirieri = srvInchirieri
        self.__comenzi = {
            "addfilm": self.__ui_adauga_film,
            "afisfilme": self.__ui_afisare_filme,
            "delfilm": self.__ui_delete_film,
            "modfilm": self.__ui_modifica_film,
            "findfilm": self.__ui_cauta_film,

            "addclient": self.__ui_adauga_client,
            "afisclienti": self.__ui_afisare_clienti,
            "delclient": self.__ui_delete_client,
            "modclient": self.__ui_modifica_client,
            "findclient": self.__ui_cauta_client,

            "inchiriaza": self.__ui_adauga_inchiriere,
            "afisinch": self.__ui_afisare_inchirieri,
            "returneaza": self.__ui_returnare,
            "ordclienti": self.__ui_inchirieri_ordonate,
            "findinch": self.__ui_cauta_inchiriere,

            "clientirandom": self.__ui_creeare_client_random,
            "filmerandom": self.__ui_creeare_filme_random,
            "inchrandom": self.__ui_creeare_inchirieri_random,

            "filmepopulare": self.__ui_cele_mai_inchiriate_filme,

            "top30%": self.__ui_top_treime_clienti,

            "filmeslabe": self.__ui_filme_slabe
        }

    def __ui_adauga_film(self, params):
        if len(params) != 4:
            raise ValueError("numar de parametrii invalid!")
        f_id = params[0]
        titlu = params[1]
        descriere = params[2]
        gen = params[3]
        try:
            f_id = int(f_id)
        except Exception:
            raise ValueError("id invalid!")
        self.__srvFilm.adauga_film(int(f_id), titlu, descriere, gen)

    def __ui_afisare_filme(self, params):
        filme = self.__srvFilm.get_filme()
        if len(filme) == 0:
            print("Nu exista filme")
        else:
            print("\nFilme:")
            for film in filme:
                print(film)
            print("\n")

    def __ui_delete_film(self, params):
        try:
            params[0] = int(params[0])
        except Exception:
            raise ValueError("id invalid!")
        self.__srvFilm.sterge_film(int(params[0]))

    def __ui_modifica_film(self, params):
        if len(params) % 2 == 0 or len(params) == 1 or len(params) > 7:
            raise ValueError ("numar parametrii invalid")
        f_id = params[0]
        titlu = 0
        descriere = 0
        gen = 0
        try:
            f_id = int(f_id)
        except Exception:
            raise ValueError("id invalid!")
        for indice in range(1, len(params), 2):
            if params[indice] == "titlu":
                titlu = params[indice + 1]
            elif params[indice] == "descriere":
                descriere = params[indice + 1]
            elif params[indice] == "gen":
                gen = params[indice + 1]
        self.__srvFilm.modifica_film(int(f_id), titlu, descriere, gen)

    def __ui_cauta_film(self, params):
        f_id = params[0]
        try:
            f_id = int(f_id)
        except Exception:
            raise ValueError("id invalid!")
        film = self.__srvFilm.cauta_film(int(f_id))
        print(film)



    def __ui_adauga_client(self, params):
        if len(params) != 3:
            raise ValueError("numar de parametrii invalid!")
        c_id = params[0]
        nume = params[1]
        cnp = params[2]
        try:
            c_id = int(c_id)
        except Exception:
            raise ValueError("id invalid!")
        self.__srvClient.adauga_client(int(c_id), nume, cnp)

    def __ui_afisare_clienti(self, params):
        clienti = self.__srvClient.get_clienti()
        if len(clienti) == 0:
            print("Nu exista clienti")
        else:
            print("\nClienti:")
            for client in clienti:
                print(client)
            print("\n")

    def __ui_delete_client(self, params):
        try:
            params[0] = int(params[0])
        except Exception:
            raise ValueError("id invalid!")
        self.__srvClient.sterge_client(int(params[0]))


    def __ui_modifica_client(self, params):
        if len(params) % 2 == 0 or len(params) == 1 or len(params) > 5:
            raise ValueError ("numar parametrii invalid")
        c_id = params[0]
        nume = 0
        cnp = 0
        try:
            c_id = int(c_id)
        except Exception:
            raise ValueError("id invalid!")
        for indice in range(1, len(params), 2):
            if params[indice] == "nume":
                nume = params[indice + 1]
            elif params[indice] == "cnp":
                cnp = params[indice + 1]
        self.__srvClient.modifica_client(int(c_id), nume, cnp)

    def __ui_cauta_client(self, params):
        c_id = params[0]
        try:
            c_id = int(c_id)
        except Exception:
            raise ValueError("id invalid!")
        client = self.__srvClient.cauta_client(int(c_id))
        print(client)




    def __ui_adauga_inchiriere(self, params):
        if len(params) != 2:
            raise ValueError ("numar parametrii invalid!")
        c_id = params[0]
        f_id = params[1]
        try:
            c_id = int(c_id)
            f_id = int(f_id)
        except Exception:
            raise ValueError("id invalid!")
        self.__srvInchirieri.adauga_inchiriere(c_id, f_id)

    def __ui_afisare_inchirieri(self, params):
        inchirieri = self.__srvInchirieri.get_inchirieri()
        if len(inchirieri) == 0:
            print("Nu exista inchirieri")
        else:
            print("\nInchirieri: client -> film")
            for inchiriere in inchirieri:
                print(inchiriere)
            print("\n")

    def __ui_returnare(self, params):
        try:
            params[0] = int(params[0])
            params[1] = int(params[1])
        except Exception:
            raise ValueError("id invalid!")
        self.__srvInchirieri.returnare(int(params[0]), int(params[1]))

    def __ui_inchirieri_ordonate(self, params):
        if len(params) == 0:
            raise ValueError("numar parametrii invalid")
        if params[0] == "nume":
            clienti = self.__srvClient.ordonare_clienti_dupa_nume()
            for i in range(0, len(clienti), 2):
                if int(clienti[i][1]) == 1:
                    print("Clientul:  ", clienti[i][0], "   A inchiriat   ", clienti[i][1], " film")
                else:
                    print("Clientul:  ", clienti[i][0], "   A inchiriat   ", clienti[i][1], " filme")
        elif params[0] == "filme":
            clienti = self.__srvClient.ordonare_clienti_dupa_nr_de_filme()
            for i in range(0, len(clienti), 2):
                if int(clienti[i][1]) == 1:
                    print("Clientul:  ", clienti[i][0], "   A inchiriat   ", clienti[i][1], " film")
                else:
                    print("Clientul:  ", clienti[i][0], "   A inchiriat   ", clienti[i][1], " filme")
        else:
            print("comanda invalida")

    def __ui_cauta_inchiriere(self, params):
        if len(params) != 2:
            raise ValueError ("numar parametrii invalid!")
        c_id = params[0]
        f_id = params[1]
        try:
            c_id = int(c_id)
            f_id = int(f_id)
        except Exception:
            raise ValueError("id invalid!")
        inchiriere = self.__srvInchirieri.cauta_inchiriere(c_id, f_id)
        print(inchiriere)





    def __ui_creeare_client_random(self, params):
        if len(params) != 1:
            raise ValueError("numar invalid de parametrii")

        minuscule = 'abcdefghijklmnopqrstuvwxyz'
        majuscule = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        numere = '0123456789'
        cifre = '123456789'

        nr_clienti = int(params[0])

        while nr_clienti > 0:
            c_id = ""
            nume = ""
            cnp = ""

            c_id += random.choice(cifre)
            for i in range(4):
                c_id += random.choice(numere)

            nume += random.choice(majuscule)
            for i in range(8):
                nume += random.choice(minuscule)

            cnp += random.choice(cifre)
            for i in range(12):
                cnp += random.choice(numere)

            adaugare = self.__srvClient.adauga_client_random(int(c_id), nume, cnp)
            if adaugare == True:
                nr_clienti -= 1

    def __ui_creeare_filme_random(self, params):
        if len(params) != 1:
            raise ValueError("numar invalid de parametrii")

        minuscule = 'abcdefghijklmnopqrstuvwxyz'
        majuscule = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        numere = '0123456789'
        cifre = '123456789'

        nr_filme = int(params[0])

        while nr_filme > 0:
            f_id = ""
            titlu = ""
            descriere = ""
            gen = ""

            f_id += random.choice(cifre)
            for i in range(4):
                f_id += random.choice(numere)

            titlu += random.choice(majuscule)
            for i in range(8):
                titlu += random.choice(minuscule)

            descriere += random.choice(majuscule)
            for i in range(8):
                descriere += random.choice(minuscule)

            gen += random.choice(majuscule)
            for i in range(8):
                gen += random.choice(minuscule)
            adaugare = self.__srvFilm.adauga_film_random(int(f_id), titlu, descriere, gen)
            if adaugare == True:
                nr_filme -= 1


    def __ui_creeare_inchirieri_random(self, params):
        if len(params) != 1:
            raise ValueError("numar invalid de parametrii")

        nr_inchirieri = int(params[0])

        inchirieri = self.__srvInchirieri.creeare_inchirieri_random()
        clienti = inchirieri[0]
        filme = inchirieri[1]

        while nr_inchirieri > 0:
            client = random.choice(clienti)
            film = random.choice(filme)

            adaugare = self.__srvInchirieri.adauga_inchiriere_random(client, film)
            if adaugare == True:
                nr_inchirieri -= 1


    def __ui_cele_mai_inchiriate_filme(self, params):
        if len(params) != 1:
            raise ValueError("numar parametrii invalid")
        nr_filme = int(params[0])
        filme_populare = self.__srvFilm.cele_mai_inchiriate_filme(nr_filme)
        i = 0
        while i < nr_filme:
            if int(filme_populare[i][2]) != 1:
                print("Filmul:  ", filme_populare[i][0], filme_populare[i][1], " a fost inchiriat de ", filme_populare[i][2], " ori")
            else:
                print("Filmul:  ", filme_populare[i][0], filme_populare[i][1], " a fost inchiriat o data")
            i += 1

    def __ui_top_treime_clienti(self, params):
        top_clienti = self.__srvClient.top_clienti()
        for i in range(0, len(top_clienti), 2):
            if top_clienti[i + 1] == 1:
                print("Clientul: ", top_clienti[i], " a inchiriat ", top_clienti[i + 1], " film")
            else:
                print("Clientul: ", top_clienti[i], " a inchiriat ", top_clienti[i + 1], " filme")


    def __ui_filme_slabe(self, params):
        if len(params) != 1:
            raise ValueError("numar parametrii invalid")
        nr_filme = int(params[0])
        filme_slabe = self.__srvFilm.cele_mai_slabe_filme(nr_filme)
        i = 0
        while i < nr_filme:
            if int(filme_slabe[i][1]) != 1:
                print("Filmul:  ", filme_slabe[i][0],  "  a fost inchiriat de  ", filme_slabe[i][1], " ori")
            else:
                print("Filmul:  ", filme_slabe[i][0], "  a fost inchiriat o data")
            i += 1


    def __afis_menu(self):
        print(
            """
            Comenzi:
                "addfilm": adauga un obiect nou de tip film in lista de filme
                "afisfilme": afiseaza toate filmele din lista
                "delfilm": sterge un film din lista de filme
                "modfilm": modifica atributele unui film
                "findfilm": cauta un film dupa id
    
                "addclient": adauga un obiect nou de tip client in lista de clienti
                "afisclienti": afiseaza toti clientii din lista
                "delclient": sterge un client din lista
                "modclient": modifica atributele unui client
                "findclient": cauta un client dupa id
    
                "inchiriaza": adauga un obiect de tip inchiriere in lista de inchirieri
                "afisinchirieri": afiseaza toate inchirierile din lista
                "returneaza": sterge o inchiriere din lista
                "ordclienti": ordoneaza clientii care au inchirieri dupa nume/numar de filme inchiriate
    
                "clientirandom": genereaza n clienti random ,n citit de la tastatura
                "filmerandom": genereaza n filme random ,n citit de la tastatura
                "inchrandom": genereaza n inchirieri random ,n citit de la tastatura
    
                "filmepopulare": afiseaza cele mai populare n filme, n citit de la tastatura
                "filmeslabe": afiseaza cele mai slab inchiriate n filme, n citit de la tastatura
    
                "top30%": afiseaza primii 30% de clienti cu cele mai multe inchirieri
            """


        )


    def run(self):
        self.__afis_menu()
        while True:
            cmd = input(">>> ")
            if len(cmd) >= 2:
                cmd = cmd.strip()
                if cmd == "exit":
                    return
                elif cmd == "menu":
                    self.__afis_menu()
                else:
                    cmd = cmd.split()
                    comanda = cmd[0]
                    params = cmd[1:]
                    if comanda in self.__comenzi:
                        try:
                            self.__comenzi[comanda](params)
                        except ValueError as vae:
                            print(str(vae))
                        except ValidError as ve:
                            print(str(ve))
                        except RepoError as re:
                            print(str(re))
                    else:
                        print("comanda invalida!")

