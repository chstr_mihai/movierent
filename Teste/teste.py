from Domain.entitati import Film, Client, Inchiriere
from validare.validatoare import ValidFilm, ValidClient, ValidInchiriere
from Infrastructura.repos import Repo
from Exceptii.exceptii import ValidError, RepoError
from Business.services import ServiceInchiriere, ServiceFilm
import unittest


class Teste:

    def __init__(self, srvInchirieri, srvFilm):
        self.__srvInchirieri = srvInchirieri
        self.__srvFilm = srvFilm


    def __test_creeaza_film(self):
        f_id = 15
        titlu = "Hobbit"
        descriere = "Frumos"
        gen = "SF"
        film = Film(f_id, titlu, descriere, gen, 0)
        assert (film.get_id() == 15)
        assert (film.get_titlu() == "Hobbit")
        film.set_titlu("Ineluse")
        assert (film.get_titlu() == "Ineluse")
        film1 = Film(15, "mama", "smek", "nu", 0)
        assert (film.__eq__(film1))
        self.__film = film

    def __test_validare_film(self):
        validFilm = ValidFilm()
        validFilm.validare_film(self.__film)
        self.__film_id_prost = Film(-2, "a", "b", "c", 0)
        self.__film_titlu_prost = Film(2, "", "b", "c", 0)
        self.__film_desc_prost = Film(2, "a", "", "c", 0)
        self.__film_gen_prost = Film(2, "a", "b", "", 0)
        self.__film_invalid = Film(-1, "", "", "", 0)
        try:
            validFilm.validare_film(self.__film_id_prost)
            assert(False)
        except Exception as ex:
            assert (str(ex) == "id invalid\n")

        try:
            validFilm.validare_film(self.__film_titlu_prost)
            assert(False)
        except Exception as ex:
            assert (str(ex) == "titlu invalid\n")

        try:
            validFilm.validare_film(self.__film_desc_prost)
            assert(False)
        except Exception as ex:
            assert (str(ex) == "descriere invalida\n")

        try:
            validFilm.validare_film(self.__film_gen_prost)
            assert(False)
        except Exception as ex:
            assert (str(ex) == "gen invalid\n")
        try:
            validFilm.validare_film(self.__film_invalid)
            assert(False)
        except Exception as ex:
            assert (str(ex) == "id invalid\ntitlu invalid\ndescriere invalida\ngen invalid\n")


    def __testIdentityFilm(self):
        film1 = Film(15, "mama", "smek", "nu", 0)
        film2 = Film(15, "amam", "kems", "un", 0)
        assert (film1 == film2)

        film2 = Film(16, "mama", "smek", "nu", 0)
        assert (film1 != film2)


    def __test_adauga_film(self):
        self.__lista_filme = Repo()
        assert (self.__lista_filme.size() == 0)
        self.__lista_filme.adauga(self.__film)
        assert (self.__lista_filme.size() == 1)
        assert (self.__lista_filme.get_all() == [self.__film])
        self.__id_film = Film(15, None, None, None, None)
        film_cautat = self.__lista_filme.cauta(self.__id_film)
        assert (film_cautat.get_titlu() == self.__film.get_titlu())
        try:
            self.__lista_filme.adauga(self.__film)
            assert(False)
        except RepoError as re:
            assert(str(re) == "exista un element cu acest id\n")

    def __test_sterge_film(self):
        self.__lista_filme = Repo()
        self.__lista_filme.adauga(self.__film)
        film1 = Film(16, "mama", "smek", "nu", 0)
        self.__lista_filme.adauga(film1)
        film2 = Film(17, "amam", "kems", "un", 0)
        self.__lista_filme.adauga(film2)
        assert (self.__lista_filme.size() == 3)
        self.__lista_filme.delete(film1)
        assert (self.__lista_filme.size() == 2)
        self.__lista_filme.delete(film2)
        assert (self.__lista_filme.size() == 1)

    def __test_creeaza_client(self):
        self.__client = Client(1, "Ana", 1111111111111, 0)
        assert (self.__client.get_id() == 1)
        assert (self.__client.get_nume() == "Ana")
        assert (self.__client.get_cnp() == 1111111111111)
        self.__client.set_nume("Maria")
        assert (self.__client.get_nume() == "Maria")
        self.__client.set_cnp(2222222222222)
        assert (self.__client.get_cnp() == 2222222222222)
        client1 = Client(1, "Dana", 1234567890123, 0)
        assert (self.__client.__eq__(client1))

    def __test_validare_client(self):
        validClient = ValidClient()
        validClient.validare_client(self.__client)
        self.__client_id_prost = Client(-1, "a", 1234567890123, 0)
        self.__client_nume_prost = Client(1, "", 1234567890123, 0)
        self.__client_cnp_prost = Client(2, "a", 123456789012, 0)
        self.__client_prost = Client(-2, "", 12345678901234, 0)
        try:
            validClient.validare_client(self.__client_id_prost)
            assert(False)
        except Exception as ex:
            assert (str(ex) == "id invalid\n")

        try:
            validClient.validare_client(self.__client_nume_prost)
            assert (False)
        except Exception as ex:
            assert (str(ex) == "nume invalid\n")

        try:
            validClient.validare_client(self.__client_cnp_prost)
            assert (False)
        except Exception as ex:
            assert (str(ex) == "cnp invalid\n")

        try:
            validClient.validare_client(self.__client_prost)
            assert (False)
        except Exception as ex:
            assert (str(ex) == "id invalid\nnume invalid\ncnp invalid\n")

    def __test_adauga_client(self):
        self.__lista_clienti = Repo()
        assert (self.__lista_clienti.size() == 0)
        self.__lista_clienti.adauga(self.__client)
        assert (self.__lista_clienti.size() == 1)
        assert (self.__lista_clienti.get_all() == [self.__client])
        self.__id_client = Client(1, None, None, None)
        client_cautat = self.__lista_clienti.cauta(self.__id_client)
        assert (client_cautat.get_nume() == self.__client.get_nume())
        try:
            self.__lista_clienti.adauga(self.__client)
            assert (False)
        except RepoError as re:
            assert (str(re) == "exista un element cu acest id\n")
        client1 = Client(2, "b", 2222222222222, 0)
        self.__lista_clienti.adauga(client1)
        assert (self.__lista_clienti.size() == 2)

    def __test_creeaza_inchiriere(self):
        client = Client(1, "Ana", 1111111111111, 0)
        film = Film(1, "a", "b", "c", 0)
        self.__inchiriere = Inchiriere(client, film)
        assert (self.__inchiriere.get_client() == client)
        assert (self.__inchiriere.get_film() == film)
        client1 = Client(2, "Maria", 2222222222222, 0)
        film1 = Film(2, "x", "y", "z", 0)
        self.__inchiriere.set_client(client1)
        assert (self.__inchiriere.get_client() == client1)
        self.__inchiriere.set_film(film1)
        assert (self.__inchiriere.get_film() == film1)
        client1 = Client(2, "Maria", 2222222222222, 0)
        film1 = Film(2, "x", "y", "z", 0)
        self.__inchiriere1 = Inchiriere(client1, film1)
        assert (self.__inchiriere.__eq__(self.__inchiriere1))


    def __test_adauga_inchiriere(self):
        self.__inchirieri = Repo()
        client = Client(2, "Maria", 2222222222222, 0)
        film = Film(2, "x", "y", "z", 0)
        client1 = Client(1, "Ana", 1234567890111, 0)
        film1 = Film(1, "a", "b", "c", 0)
        self.__inchiriere.set_client(client1)
        self.__inchiriere.set_film(film1)
        assert (self.__inchirieri.size() == 0)
        self.__inchirieri.adauga(self.__inchiriere)
        assert (self.__inchirieri.size() == 1)
        assert (self.__inchirieri.get_all() == [self.__inchiriere])
        self.__inchirieri.adauga(self.__inchiriere1)
        assert (self.__inchirieri.size() == 2)
        id_inch = Inchiriere(Client(2, None, None, None), Film(2, None, None, None, None))
        inch_cautata = self.__inchirieri.cauta(id_inch)
        assert (inch_cautata.get_client() == client)
        assert (inch_cautata.get_film() == film)
        try:
            self.__inchirieri.adauga(self.__inchiriere)
            assert (False)
        except Exception as ie:
            assert (str(ie) == "exista un element cu acest id\n")


    def __test_sterge_client(self):
        self.__lista_clienti = Repo()
        self.__client1 = (1, "a", 1234567890111)
        self.__client2 = (2, "b", 3216549870111)
        self.__client3 = (3, "c", 3698521470222)
        self.__lista_clienti.adauga(self.__client1)
        self.__lista_clienti.adauga(self.__client2)
        self.__lista_clienti.adauga(self.__client3)
        assert (self.__lista_clienti.size() == 3)
        self.__lista_clienti.delete(self.__client2)
        assert (self.__lista_clienti.size() == 2)
        try:
            self.__lista_clienti.cauta(self.__client2)
        except Exception as ex:
            assert (str(ex) == "id inexistent!\n")

    def __test_filme_slabe(self):
        self.__lista_filme = Repo()
        film1 = Film(1, "b", "b", "c", 5)
        film2 = Film(2, "d", "e", "f", 1)
        film3 = Film(3, "a", "h", "i", 1)
        film4 = Film(4, "a", "g", "z", 5)
        film5 = Film(5, "x", "y", "z", 0)
        self.__lista_filme.adauga(film1)
        self.__lista_filme.adauga(film2)
        self.__lista_filme.adauga(film3)
        self.__lista_filme.adauga(film4)
        self.__lista_filme.adauga(film5)
        assert (self.__srvFilm.cele_mai_slabe_filme(5) == [["x", 0], ["a", 1], ["d", 1], ["a", 5], ["b", 5]])




    def run_all_tests(self):
        self.__test_creeaza_film()
        self.__test_validare_film()
        self.__test_adauga_film()
        self.__testIdentityFilm()
        self.__test_sterge_film()
        self.__test_creeaza_client()
        self.__test_validare_client()
        self.__test_adauga_client()
        self.__test_creeaza_inchiriere()
        self.__test_sterge_client()
        self.__test_adauga_inchiriere()


class unittestTests(unittest.TestCase):


    def setUp(self):
        unittest.TestCase.setUp(self)

    def tearDown(self):
        unittest.TestCase.tearDown(self)

    def testCreateClient(self):
        client1 = Client(1, "Ion", 5001229172347, 0)
        self.assertEqual(client1.get_id(), 1)
        self.assertEqual(client1.get_nume(), "Ion")
        self.assertEqual(client1.get_cnp(),5001229172347)
        client1.set_nume("Vasile")
        self.assertEqual(client1.get_nume(), "Vasile")
        client1.set_cnp(1231231231231)
        self.assertEqual(client1.get_cnp(), 1231231231231)
        self.__client = client1

    def testValidClient(self):
        validClient = ValidClient()
        self.__client_id_invalid = Client(-1, "Ion", 1231231231231, 0)
        self.__client_nume_invalid = Client(1, "", 1231231231231, 0)
        self.__client_cnp_invalid1 = Client(1, "Ion", 1, 0)
        self.__client_cnp_invalid2 = Client(1, "Ion", 12345678901234, 0)
        with self.assertRaises(ValidError):
            validClient.validare_client(self.__client_id_invalid)
            validClient.validare_client(self.__client_nume_invalid)
            validClient.validare_client(self.__client_cnp_invalid1)
            validClient.validare_client(self.__client_cnp_invalid2)
        self.__validClient = validClient

    def testRepoClient(self):
        cli = Client(1, "Ion", 5001229172347, 0)
        self.__repoClient = Repo()
        self.__repoClient.adauga(cli)
        client = Client(1, "Vasile", 1234567890123, 0)
        client1 = Client(2, "Sorin", 1231231231231, 0)
        client3 = Client(1, None, None, 0)
        client4 = Client(None, "Vasile", None, 0)
        with self.assertRaises(RepoError):
            self.__repoClient.adauga(client)
        self.assertEqual(len(self.__repoClient.get_all()), 1)

        with self.assertRaises(RepoError):
            self.__repoClient.cauta(client1)
        self.__repoClient.delete(cli)
        with self.assertRaises(RepoError):
            self.__repoClient.delete(client1)
        self.assertEqual(len(self.__repoClient.get_all()), 0)

    def testCreateFilm(self):
        film1 = Film(1, 'Titlu', 'Descriere', 'Gen', 0)
        self.assertEqual(film1.get_id(), 1)
        self.assertEqual(film1.get_titlu(), "Titlu")
        self.assertEqual(film1.get_descriere(), "Descriere")
        self.assertEqual(film1.get_gen(), "Gen")
        film1.set_titlu("AltTitlu")
        self.assertEqual(film1.get_titlu(), "AltTitlu")
        film1.set_descriere("AltaDescriere")
        self.assertEqual(film1.get_descriere(), "AltaDescriere")
        film1.set_gen("AltGen")
        self.assertEqual(film1.get_gen(), "AltGen")
        self.__film = film1

    def testValidFilm(self):
        validFilme = ValidFilm()
        self.__film_id_invalid = Film(-1, 'a', 'a','a', 0)
        self.__film_titlu_invalid = Film(1, '', 'a', 'a', 0)
        self.__film_descriere_invalida = Film(1, 'a', '', 'a', 0)
        self.__film_gen_invalid = Film(1, 'a', 'a', '', 0)
        with self.assertRaises(ValidError):
            validFilme.validare_film(self.__film_id_invalid)
            validFilme.validare_film(self.__film_titlu_invalid)
            validFilme.validare_film(self.__film_descriere_invalida)
            validFilme.validare_film(self.__film_gen_invalid)
        self.__validFilme = validFilme

    def testRepoFilme(self):
        film = Film(1, 'Titlu', 'Descriere', 'Gen', 0)
        self.__repoFilme = Repo()
        self.__repoFilme.adauga(film)
        film1 = Film(1, 'b', 'b', 'b', 0)
        film2 = Film(1, None, None, None, 0)
        film3 = Film(2, None, None, None, 0)
        with self.assertRaises(RepoError):
            self.__repoFilme.adauga(film1)
        self.assertEqual(len(self.__repoFilme.get_all()), 1)

        with self.assertRaises(RepoError):
            self.__repoFilme.cauta(film3)
        with self.assertRaises(RepoError):
            self.__repoFilme.delete(film3)
        self.__repoFilme.delete(film1)
        self.assertEqual(len(self.__repoFilme.get_all()), 0)

    def testRepoInchirieri(self):
        self.__repoInchirieri = Repo()
        film1 = Film(1, 'Titlu', 'Descriere', 'Gen', 0)
        client1 = Client(1, "Ion", 5001229172347, 0)
        inchiriere = Inchiriere(film1, client1)
        self.__repoInchirieri.adauga(inchiriere)
        self.assertEqual(len(self.__repoInchirieri.get_all()), 1)
        self.__repoInchirieri.delete(inchiriere)
        self.assertEqual(len(self.__repoInchirieri.get_all()), 0)

    def run_tests(self):
        self.testCreateClient()
        self.testValidClient()
        self.testRepoClient()
        self.testCreateFilm()
        self.testValidFilm()
        self.testRepoFilme()
        self.testRepoInchirieri()
