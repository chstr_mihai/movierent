from Domain.entitati import Film, Client, Inchiriere
from Business.sortari import *



class ServiceFilm:

    def __init__(self, repoFilm, validFilm):
        self.__repoFilm = repoFilm
        self.__validFilm = validFilm

    def adauga_film(self, f_id, titlu, descriere, gen):
        #functia care adauga un film in lista de filme
        film = Film(f_id, titlu, descriere, gen, 0)
        self.__validFilm.validare_film(film)
        self.__repoFilm.adauga(film)



    def adauga_film_random(self, f_id, titlu, descriere, gen):
        #functia care adauga un film in lista de filme
        film = Film(f_id, titlu, descriere, gen, 0)
        self.__validFilm.validare_film(film)
        try:
            self.__repoFilm.adauga(film)
        except Exception:
            return False
        return True


    def get_filme(self):
        #functia care intoarce o lista care contine toate filmele
        return self.__repoFilm.get_all()

    def sterge_film(self, f_id):
        #functia care sterge un film din lista
        f = Film(f_id, None, None, None, None)
        x = self.__repoFilm.size()
        film = self.__repoFilm.cauta(f, x - 1)
        self.__repoFilm.delete(film, x - 1)

    def modifica_film(self, f_id, titluNou, descriereNoua, genNou):
        #functia care modifica atributele unui film
        f = Film(f_id, None, None, None, None)
        x = self.__repoFilm.size()
        film = self.__repoFilm.cauta(f, x - 1)
        if titluNou != 0:
            film.set_titlu(titluNou)
        if descriereNoua != 0:
            film.set_descriere(descriereNoua)
        if genNou != 0:
            film.set_gen(genNou)
        self.__repoFilm.modify(film)

    def cauta_film(self, f_id):
        #functia care cauta un film in lista
        film = Film(f_id, None, None, None, None)
        x = self.__repoFilm.size()
        return self.__repoFilm.cauta(film, x - 1)

    def cele_mai_inchiriate_filme(self, nr_filme):
        #functia care returneaza cele mai inchiriate n filme
        sortare=Sortari()
        filme = self.__repoFilm.get_all()
        nr_inchirieri = []
        for f in filme:
            if f.get_nr_inchirieri() != 0:
                nr_inchirieri.append(f.get_nr_inchirieri())
        nr_inchirieri = sortare.shake_sort(nr_inchirieri, True)
        if nr_filme > len(nr_inchirieri):
            raise ValueError("numarul de filme inchiriate este prea mic")
        nr_inchirieri = list(dict.fromkeys(nr_inchirieri))
        filme_dupa_nr_inchirieri = []
        contor = 0
        for nr in nr_inchirieri:
            for f in filme:
                if f.get_nr_inchirieri() == nr:
                    filme_dupa_nr_inchirieri.append([f.get_id(), f.get_titlu(), str(f.get_nr_inchirieri())])
                    contor += 1
                    if contor > nr_filme:
                        return filme_dupa_nr_inchirieri

    def cele_mai_slabe_filme(self, nr_filme):
        #functia care returneaza cele mai putin inchiriate n filme
        sortare=Sortari()
        filme = self.__repoFilm.get_all()
        nr_inchirieri = []
        for f in filme:
            nr_inchirieri.append([f.get_nr_inchirieri(), f.get_titlu()])
            nr_inchirieri = sortare.selection_sort(nr_inchirieri, False)
        contor = 0
        if nr_filme > len(nr_inchirieri):
            raise ValueError("numarul de filme inchiriate este prea mic")
        filme_dupa_nr_inchiririeri = []
        for elem in nr_inchirieri:
            for f in filme:
                if f.get_nr_inchirieri() == elem[0] and f.get_titlu() == elem[1]:
                    filme_dupa_nr_inchiririeri.append([f.get_titlu(), int(f.get_nr_inchirieri())])
                    contor += 1
                    if contor == nr_filme:
                        return filme_dupa_nr_inchiririeri



class ServiceClient:

    def __init__(self, repoClient, validClient):
        self.__repoClient = repoClient
        self.__validClient = validClient

    def adauga_client(self, c_id, nume, cnp):
        #functia care adauga un client nou in lista de clienti
        client = Client(c_id, nume, cnp, 0)
        self.__validClient.validare_client(client)
        self.__repoClient.adauga(client)


    def adauga_client_random(self, c_id, nume, cnp):
        #functia care adauga un film in lista de filme
        client = Client(c_id, nume, cnp, 0)
        self.__validClient.validare_client(client)
        try:
            self.__repoClient.adauga(client)
        except Exception:
            return False
        return True


    def get_clienti(self):
        #functia care returneaza o lista cu toti clientii
        return self.__repoClient.get_all()

    def sterge_client(self, c_id):
        #functia care sterge un client din lista
        c = Client(c_id, None, None, None)
        x = self.__repoClient.size()
        client = self.__repoClient.cauta(c, x - 1)
        self.__repoClient.delete(client, x - 1)

    def modifica_client(self, c_id, numeNou, cnpNou):
        #functia care modifica atributele unui client
        c = Client(c_id, None, None, None)
        x = self.__repoClient.size()
        client = self.__repoClient.cauta(c, x - 1)
        if numeNou != 0:
           client.set_nume(numeNou)
        if cnpNou != 0:
            client.set_cnp(cnpNou)

    def cauta_client(self, c_id):

        #functia care cauta un client in lista de clienti
        client = Client(c_id, None, None, None)
        x = self.__repoClient.size()
        return self.__repoClient.cauta(client, x - 1)

    def top_clienti(self):
        sortare = Sortari()
        #functia care returneaza o lista continand primii 30% de clienti cu cele mai multe filme inchiriate
        clienti = self.__repoClient.get_all()
        nr_clienti = int(30/100 * len(clienti))
        nr_inchirieri = []
        for c in clienti:
            nr_inchirieri.append(c.get_nr_filme())
        nr_inchirieri = sortare.selection_sort(nr_inchirieri, True)
        i = 0
        nr_inchirieri = list(dict.fromkeys(nr_inchirieri))
        clienti_dupa_nr_filme = []
        contor = 0
        for nr in nr_inchirieri:
            for c in clienti:
                if c.get_nr_filme() == nr:
                    clienti_dupa_nr_filme.append(c.get_nume())
                    clienti_dupa_nr_filme.append(str(c.get_nr_filme()))
                    contor += 1
                    if contor == nr_clienti:
                        return clienti_dupa_nr_filme

    def ordonare_clienti_dupa_nume(self):
        #functia care returneaza o lista numele si numarul de film inchiriate, ordonati dupa nume
        sortare = Sortari()
        nume = []
        clienti_ordonati_dupa_nume = []
        clienti = self.__repoClient.get_all()
        for c in clienti:
            if c.get_nr_filme() != 0:
                nume.append(c.get_nume())
        nume = sortare.selection_sort(nume, False)
        nume = list(dict.fromkeys(nume))
        for n in nume:
            for c in clienti:
                if c.get_nume() == n:
                    clienti_ordonati_dupa_nume.append([c.get_nume(), str(c.get_nr_filme())])
        return clienti_ordonati_dupa_nume


    def ordonare_clienti_dupa_nr_de_filme(self):
        sortare = Sortari()
        nr_filme = []
        clienti_ordonati_dupa_nr_de_filme = []
        clienti = self.__repoClient.get_all()
        for c in clienti:
            if c.get_nr_filme() != 0:
                nr_filme.append(c.get_nr_filme())
        nr_filme = sortare.selection_sort(nr_filme, True)
        nr_filme = list(dict.fromkeys(nr_filme))
        for nr in nr_filme:
            for c in clienti:
                if c.get_nr_filme() == nr:
                    clienti_ordonati_dupa_nr_de_filme.append([c.get_nume(), str(c.get_nr_filme())])
        return clienti_ordonati_dupa_nr_de_filme



class ServiceInchiriere:

    def __init__(self, repoInchirieri, validInchirieri, repoClient, repoFilm):
        self.__repoInchirieri = repoInchirieri
        self.__validInchirieri = validInchirieri
        self.__repoClient = repoClient
        self.__repoFilm = repoFilm

    def adauga_inchiriere(self, c_id, f_id):
        #functia care adauga un obiect de tip inchiriere in lista de inchirieri
        c = Client(c_id, None, None, None)
        f = Film(f_id, None, None, None, None)
        x = self.__repoClient.size()
        client = self.__repoClient.cauta(c, x - 1)
        y = self.__repoFilm.size()
        film = self.__repoFilm.cauta(f, y - 1)
        inchiriere = Inchiriere(client, film)
        self.__repoInchirieri.adauga(inchiriere)
        client.set_nr_filme(client.get_nr_filme() + 1)
        film.set_nr_inchirieri(film.get_nr_inchirieri() + 1)

    def adauga_inchiriere_random(self, c_id, f_id):
        #functia care adauga un obiect de tip inchiriere in lista de inchirieri
        c = Client(c_id, None, None, None)
        f = Film(f_id, None, None, None, None)
        x = self.__repoFilm.size()
        client = self.__repoClient.cauta(c, x - 1)
        y =  self.__repoFilm.size()
        film = self.__repoFilm.cauta(f, y - 1)
        inchiriere = Inchiriere(client, film)
        try:
            self.__repoInchirieri.adauga(inchiriere)
        except Exception:
            return False
        client.set_nr_filme(client.get_nr_filme() + 1)
        film.set_nr_inchirieri(film.get_nr_inchirieri() + 1)
        return True

    def get_inchirieri(self):
        #functia care returneaza o lista continand toate inchirierile
        return self.__repoInchirieri.get_all()

    def returnare(self, c_id, f_id):
        #functia care sterge o inchiriere din lista
        c = Client(c_id, None, None, None)
        f = Film(f_id, None, None, None, None)
        x = self.__repoClient.size()
        y = self.__repoFilm.size()
        client = self.__repoClient.cauta(c, x - 1)
        film = self.__repoFilm.cauta(f, y - 1)
        inchiriere = Inchiriere(client, film)
        z = self.__repoInchirieri.size()
        self.__repoInchirieri.delete(inchiriere, z - 1)
        client.set_nr_filme(client.get_nr_filme() - 1)
        film.set_nr_inchirieri(film.get_nr_inchirieri() - 1)

    def creeare_inchirieri_random(self):
        #functia care creeaza n inchirieri random
        inchirieri = []
        id_clienti = []
        id_filme = []
        clienti = self.__repoClient.get_all()
        filme = self.__repoFilm.get_all()
        for c in clienti:
            id_clienti.append(c.get_id())
        for f in filme:
            id_filme.append(f.get_id())
        inchirieri.append(id_clienti)
        inchirieri.append(id_filme)
        return inchirieri

    def cauta_inchiriere(self, c_id, f_id):
        client = Client(c_id, None, None, None)
        film = Film(f_id, None, None, None, None)
        inchiriere = Inchiriere(client, film)
        x = self.__repoInchirieri.size()
        return self.__repoInchirieri.cauta(inchiriere, x - 1)

