from Infrastructura.repos import Repo


class Sortari:

    def __init__(self):
        self.cmp = Repo.cmp

    def selection_sort(self, lista, reverse):
        """
        Sorteaza o lista crescator/descrescator in functie de cheie
        Input:
            - reverse: arata directia de sortare
            - lista: lista de obiecte
        """
        if reverse == True:
            for i in range(len(lista) - 1):
                for j in range(i, len(lista)):
                    if self.cmp(lista[i], lista[j]) >= 0:
                        aux = lista[i]
                        lista[i] = lista[j]
                        lista[j] = aux
        else:
            for i in range(len(lista) - 1):
                for j in range(i, len(lista)):
                    if self.cmp(lista[i], lista[j]) == -1:
                        aux = lista[i]
                        lista[i] = lista[j]
                        lista[j] = aux

        return lista

    def shake_sort(self, lista, reverse):
        """
        Sorteaza o lista crescator/descrescator in functie de cheie
        Input:
            - reverse: arata directia de sortare
            - lista: lista de obiecte
        """
        ok = 1
        if reverse == True:
            while ok == 1:
                ok = 0
                for i in range(len(lista) - 1):
                    if lista[i] < lista[i + 1]:
                        aux = lista[i]
                        lista[i] = lista[i + 1]
                        lista[i + 1] = aux
                        ok = 1
                if ok == 0:
                    return lista

                for i in range(len(lista) - 2, 0, -1):
                    if lista[i] < lista[i + 1]:
                        aux = lista[i]
                        lista[i] = lista[i + 1]
                        lista[i + 1] = aux

        else:
            while ok == 1:
                ok = 0
                for i in range(len(lista) - 1):
                    if lista[i] > lista[i + 1]:
                        aux = lista[i]
                        lista[i] = lista[i + 1]
                        lista[i + 1] = aux
                        ok = 1
                if ok == 0:
                    return lista

                for i in range(len(lista) - 2, 0, -1):
                    if lista[i] > lista[i + 1]:
                        aux = lista[i]
                        lista[i] = lista[i + 1]
                        lista[i + 1] = aux

        return lista

