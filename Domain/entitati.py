class Film:

    def __init__(self, f_id, titlu, descriere, gen, nr_inchirieri):
        self.__f_id= f_id
        self.__titlu = titlu
        self.__descriere = descriere
        self.__gen = gen
        self.__nr_inchirieri = nr_inchirieri

    def get_id(self):
        return self.__f_id

    def get_titlu(self):
        return self.__titlu

    def get_descriere(self):
        return self.__descriere

    def get_gen(self):
        return self.__gen

    def get_nr_inchirieri(self):
        return self.__nr_inchirieri

    def set_titlu(self, value):
        self.__titlu = value

    def set_descriere(self, value):
        self.__descriere = value

    def set_gen(self, value):
        self.__gen = value

    def set_nr_inchirieri(self, value):
        self.__nr_inchirieri = value

    def __eq__(self, other):
        return self.__f_id == other.__f_id

    def __str__(self):
        return str(self.__f_id) + " " + self.__titlu + " " + self.__descriere + " " + self.__gen


class Client:

    def __init__(self, c_id, nume, cnp, nr_filme):
        self.__c_id = c_id
        self.__nume = nume
        self.__cnp = cnp
        self.__nr_filme = nr_filme

    def get_id(self):
        return self.__c_id

    def get_nume(self):
        return self.__nume

    def get_cnp(self):
        return self.__cnp

    def get_nr_filme(self):
        return self.__nr_filme

    def set_nume(self, value):
        self.__nume = value

    def set_cnp(self, value):
        self.__cnp = value

    def set_nr_filme(self, value):
        self.__nr_filme = value

    def __eq__(self, other):
        return self.__c_id == other.__c_id

    def __str__(self):
        return str(self.__c_id) + " " + self.__nume + " " + self.__cnp


class Inchiriere:

    def __init__(self, client, film):
        self.__client = client
        self.__film = film

    def get_film(self):
        return self.__film

    def get_client(self):
        return self.__client

    def set_film(self, ob):
        self.__film = ob

    def set_client(self, ob):
        self.__client = ob

    def __eq__(self, other):
        return self.__client == other.__client and self.__film == other.__film

    def __str__(self):
        return "Clientul:   " + str(self.__client) + "   A inchiriat filmul:   " + str(self.__film)