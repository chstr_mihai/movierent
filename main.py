from Teste.teste import Teste, unittestTests
from Infrastructura.repos import Repo
from validare.validatoare import ValidFilm, ValidClient, ValidInchiriere
from Business.services import ServiceFilm, ServiceClient, ServiceInchiriere
from Presentation.console import Console
from Infrastructura.fileRepos import FilmRepoFromFile, ClientRepoFromFile, InchiriereRepoFromFile

repoFilm = FilmRepoFromFile("filme")
repoClient = ClientRepoFromFile("clienti1")
repoInchirieri = Repo()
validFilm = ValidFilm()
validClient = ValidClient()
validInchirieri = ValidInchiriere()
srvFilm = ServiceFilm(repoFilm, validFilm)
srvClient = ServiceClient(repoClient, validClient)
srvInchirieri = ServiceInchiriere(repoInchirieri, validInchirieri, repoClient, repoFilm)
ui = Console(srvFilm, srvClient, srvInchirieri)
teste = Teste(srvInchirieri, srvFilm)
utests = unittestTests()

#teste.run_all_tests()
#utests.run_tests()
ui.run()
