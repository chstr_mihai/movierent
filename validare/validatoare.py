from Exceptii.exceptii import ValidError

class ValidFilm:

    def __init__(self):
        pass

    def validare_film(self, film):
        erori = ""
        if int(film.get_id()) < 0:
            erori += "id invalid\n"
        if film.get_titlu() == "":
            erori += "titlu invalid\n"
        if film.get_descriere() == "":
            erori += "descriere invalida\n"
        if film.get_gen() == "":
            erori += "gen invalid\n"
        if len(erori) > 0:
            raise ValidError(erori)

class ValidClient:

    def __init__(self):
        pass

    def validare_client(self, client):
        erori = ""
        if int(client.get_id()) < 0:
            erori += "id invalid\n"
        if client.get_nume() == "":
            erori += "nume invalid\n"
        if int(client.get_cnp()) < 1000000000000 or int(client.get_cnp()) > 10000000000000:
            erori += "cnp invalid\n"
        if len(erori) > 0:
            raise ValidError(erori)

class ValidInchiriere:

    def __init__(self):
        pass