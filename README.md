# README #

A python based console application used for a movie renting client.

The main functionalities of the app are:
    
    - add/delete/modify/find/print  client/movie
    
    - rent/return movie
    
    - find/print movie rent
    
    - find most popular/non-popular x movies